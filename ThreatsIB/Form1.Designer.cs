﻿namespace ThreatsIB
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.CreateBase = new System.Windows.Forms.Button();
            this.UpdateBase = new System.Windows.Forms.Button();
            this.ViewBase = new System.Windows.Forms.Button();
            this.ViewThreat = new System.Windows.Forms.Button();
            this.SaveBase = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(0, 0);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(1600, 1024);
            this.textBox1.TabIndex = 0;
            // 
            // CreateBase
            // 
            this.CreateBase.Location = new System.Drawing.Point(1630, 20);
            this.CreateBase.Name = "CreateBase";
            this.CreateBase.Size = new System.Drawing.Size(250, 90);
            this.CreateBase.TabIndex = 1;
            this.CreateBase.Text = "Создать базу";
            this.CreateBase.UseVisualStyleBackColor = true;
            this.CreateBase.Click += new System.EventHandler(this.CreateBase_Click);
            // 
            // UpdateBase
            // 
            this.UpdateBase.Location = new System.Drawing.Point(1630, 130);
            this.UpdateBase.Name = "UpdateBase";
            this.UpdateBase.Size = new System.Drawing.Size(250, 90);
            this.UpdateBase.TabIndex = 2;
            this.UpdateBase.Text = "Обновить базу";
            this.UpdateBase.UseVisualStyleBackColor = true;
            this.UpdateBase.Click += new System.EventHandler(this.UpdateBase_Click);
            // 
            // ViewBase
            // 
            this.ViewBase.Location = new System.Drawing.Point(1630, 250);
            this.ViewBase.Name = "ViewBase";
            this.ViewBase.Size = new System.Drawing.Size(250, 90);
            this.ViewBase.TabIndex = 3;
            this.ViewBase.Text = "Посмотреть локальную базу";
            this.ViewBase.UseVisualStyleBackColor = true;
            this.ViewBase.Click += new System.EventHandler(this.ViewBase_Click);
            // 
            // ViewThreat
            // 
            this.ViewThreat.Location = new System.Drawing.Point(1630, 380);
            this.ViewThreat.Name = "ViewThreat";
            this.ViewThreat.Size = new System.Drawing.Size(250, 90);
            this.ViewThreat.TabIndex = 4;
            this.ViewThreat.Text = "Посмотреть угрозу";
            this.ViewThreat.UseVisualStyleBackColor = true;
            this.ViewThreat.Click += new System.EventHandler(this.ViewThreat_Click);
            // 
            // SaveBase
            // 
            this.SaveBase.Location = new System.Drawing.Point(1630, 510);
            this.SaveBase.Name = "SaveBase";
            this.SaveBase.Size = new System.Drawing.Size(250, 90);
            this.SaveBase.TabIndex = 5;
            this.SaveBase.Text = "Сохранить базу";
            this.SaveBase.UseVisualStyleBackColor = true;
            this.SaveBase.Click += new System.EventHandler(this.SaveBase_Click);
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.White;
            this.textBox2.Location = new System.Drawing.Point(1630, 476);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(250, 26);
            this.textBox2.TabIndex = 6;
            this.textBox2.Text = "Введите номер угрозы";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1898, 1024);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.SaveBase);
            this.Controls.Add(this.ViewThreat);
            this.Controls.Add(this.ViewBase);
            this.Controls.Add(this.UpdateBase);
            this.Controls.Add(this.CreateBase);
            this.Controls.Add(this.textBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button CreateBase;
        private System.Windows.Forms.Button UpdateBase;
        private System.Windows.Forms.Button ViewBase;
        private System.Windows.Forms.Button ViewThreat;
        private System.Windows.Forms.Button SaveBase;
        private System.Windows.Forms.TextBox textBox2;
    }
}

