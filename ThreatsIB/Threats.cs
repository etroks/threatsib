﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using Excel = Microsoft.Office.Interop.Excel;

namespace ThreatsIB
{
    class Threats
    {
        private string pathTxt = Directory.GetCurrentDirectory() + @"\" + "localThreats.txt";
        private string pathXlsl = Directory.GetCurrentDirectory() + @"\" + "threats.xlsx";
        private Dictionary<int, string[]> threatList = new Dictionary<int, string[]>();

        public Dictionary<int, string[]> ThreatList { get => threatList; }

        public Threats()
        {
            Load();
        }

        public void Save()
        {
            using (StreamWriter sw = new StreamWriter(pathTxt))
            {
                sw.Write(this.ToString());
            }
        }

        public void Load()
        {
            threatList.Clear();
            threatList = LoadTxt();
        }

        private Dictionary<int, string[]> LoadTxt()
        {
            Dictionary<int, string[]> dic = new Dictionary<int, string[]>();
            try
            {
                using (StreamReader sr = new StreamReader(pathTxt))
                {
                    while (sr.Peek() > 0)
                    {
                        dic.Add(Convert.ToInt32(sr.ReadLine()), new string[7] { sr.ReadLine(), sr.ReadLine(), sr.ReadLine(), sr.ReadLine(), sr.ReadLine(), sr.ReadLine(), sr.ReadLine() });
                    }
                }
                return dic;
            }
            catch
            {
                dic.Clear();
                dic.Add(1, new string[1] { "Ошибка загрузки." });
                return dic;
            };

        }

        public string Update()
        {
            Dictionary<int, string[]> dic = new Dictionary<int, string[]>();
            Dictionary<int, string[]> updates = new Dictionary<int, string[]>();
            bool ifUpdate = false;
            dic = LoadTxt();
            DownloadThreats();
            ReadXlsx();
            foreach (var lists in threatList)
            {
                if (dic.ContainsKey(lists.Key))
                {
                    string[] arr = new string[7];
                    for (int i = 0; i < 7; i++)
                    {
                        if (dic[lists.Key][i].Replace("\r\n", "") != lists.Value[i].Replace("\r\n", ""))
                        {
                            arr[i] = ("БЫЛО:" + "\r\n" + dic[lists.Key][i] + "\r\n" + "СТАЛО:\r\n" + lists.Value[i]);
                            ifUpdate = true;
                        }
                    }
                    if (ifUpdate) updates.Add(lists.Key, arr);
                    ifUpdate = false;
                }
                else
                {
                    updates.Add(lists.Key, lists.Value);
                }
            }
            Save();
            return UpdateToString(updates);
        }

        public void DownloadThreats()
        {
            WebClient wc = new WebClient();
            wc.DownloadFileAsync(new Uri("https://bdu.fstec.ru/documents/files/thrlist.xlsx"), pathXlsl);
        }

        public void ReadXlsx()
        {
            Excel.Application ObjWorkExcel = new Excel.Application(); //открыть эксель
            Excel.Workbook ObjWorkBook = ObjWorkExcel.Workbooks.Open(pathXlsl); //открыть файл
            Excel.Worksheet ObjWorkSheet = (Excel.Worksheet)ObjWorkBook.Sheets[1]; //получить 1 лист
            var lastCell = ObjWorkSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell);//1 ячейку
            threatList.Clear();
            string value;
            for (int j = 2; j < lastCell.Row; j++)
            { // по всем строкам
                string[] arr = new string[7];
                for (int i = 1; i < lastCell.Column - 2; i++) //по всем колонкам
                {
                    value = ObjWorkSheet.Cells[j + 1, i + 1].Text.ToString();//считываем текст в строку
                    switch (value)
                    {
                        case "1":
                            arr[i - 1] = "Да";
                            break;
                        case "0":
                            arr[i - 1] = "Нет";
                            break;
                        default:
                            arr[i - 1] = value;
                            break;
                    }
                }
                threatList.Add(j - 1, arr);
            }
            ObjWorkBook.Close(); //закрыть не сохраняя
            ObjWorkExcel.Quit(); // выйти из экселя
            GC.Collect(); // убрать за собой
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            foreach (var lists in threatList)
            {
                str.Append(lists.Key + "\r\n");
                foreach (var entry in lists.Value)
                {
                    str.Append(entry.Replace(Environment.NewLine, string.Empty) + "\r\n");
                }
            }
            return str.ToString();
        }
        public string ToStringOneThreat(int a)
        {
            string str = null;
            foreach (var thrat in threatList[a])
                str += (thrat + "\r\n");
            return str;
        }
        public string ToStringShortened()
        {
            StringBuilder str = new StringBuilder();
            foreach (var lists in threatList)
                str.Append("УБИ." + lists.Key + "   " + lists.Value[0] + "\r\n");
            return str.ToString();
        }
        private string UpdateToString(Dictionary<int, string[]> dic)
        {
            StringBuilder str = new StringBuilder();
            str.Append("Обновление прошло успешно!" + "\r\n" + "Изменено " + dic.Count +
                (((dic.Count == 1) || (dic.Count == 2) || (dic.Count == 3) || (dic.Count == 4)) ? (" записи.") : (" записей.")) + "\r\n");
            foreach (var lists in dic)
            {
                str.Append(lists.Key + " запись изменена" + "\r\n");
                foreach (var entry in lists.Value)
                {
                    if (entry != null)
                        str.Append(entry.Replace(Environment.NewLine, string.Empty) + "\r\n");
                }
            }
            return str.ToString();
        }
    }
}