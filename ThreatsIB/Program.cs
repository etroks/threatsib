﻿using System;
using System.Windows.Forms;

namespace ThreatsIB
{
    static class Program
    {
        //public static Threats threats = new Threats(@"D:\threats.xlsx");
        public static Threats threats = new Threats();
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
            

        }
    }
}
