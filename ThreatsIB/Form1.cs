﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThreatsIB
{
    public partial class Form1 : Form
    {
        private int threatChanged = -1;
        public Form1()
        {
            InitializeComponent();
        }

        private void CreateBase_Click(object sender, EventArgs e)
        {
            Program.threats.DownloadThreats();
            Program.threats.ReadXlsx();
            Program.threats.Save();
        }

        private void ViewThreat_Click(object sender, EventArgs e)
        {
            try { 
            textBox1.Text = Program.threats.ToStringOneThreat(threatChanged);}
            catch { textBox1.Text = "Угрозы с данным индексом не существует"; }
        }

        private void ViewBase_Click(object sender, EventArgs e)
        {
            Program.threats.Load();
            textBox1.Text = Program.threats.ToStringShortened();
        }

        private void UpdateBase_Click(object sender, EventArgs e)
        {
            textBox1.Text = Program.threats.Update().ToString();
        }

        private void SaveBase_Click(object sender, EventArgs e)
        {
            //Program.threats.ReadXlsx();
            Program.threats.Save();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            try {
            threatChanged = Convert.ToInt32(textBox2.Text);
                //if (threatchanged < 0) { threatchanged = -1;
                //    throw exception e;
                //}
            }
            catch { textBox1.Text = "Ошибка ввода номера угрозы"; }
        }
    }
}
